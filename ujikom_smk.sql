-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 06 Apr 2019 pada 15.37
-- Versi Server: 5.5.36
-- PHP Version: 5.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ujikom_smk`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_smntr`
--

CREATE TABLE IF NOT EXISTS `data_smntr` (
  `id_detail_pinjam` int(11) NOT NULL,
  `id_inventaris` int(20) NOT NULL,
  `jumlah_pinjam` varchar(25) NOT NULL,
  PRIMARY KEY (`id_detail_pinjam`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_pinjam`
--

CREATE TABLE IF NOT EXISTS `detail_pinjam` (
  `id_detail_pinjam` int(11) NOT NULL AUTO_INCREMENT,
  `id_inventaris` int(11) NOT NULL,
  `jumlah_pinjam` varchar(30) NOT NULL,
  `id_peminjaman` int(11) NOT NULL,
  PRIMARY KEY (`id_detail_pinjam`),
  KEY `id_peminjaman` (`id_peminjaman`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data untuk tabel `detail_pinjam`
--

INSERT INTO `detail_pinjam` (`id_detail_pinjam`, `id_inventaris`, `jumlah_pinjam`, `id_peminjaman`) VALUES
(22, 24, '5', 39),
(23, 25, '5', 40),
(24, 22, '5', 41),
(25, 23, '5', 42),
(26, 24, '5', 42),
(27, 23, '5', 42),
(28, 24, '5', 42),
(29, 24, '5', 46),
(30, 47, '', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `inventaris`
--

CREATE TABLE IF NOT EXISTS `inventaris` (
  `id_inventaris` int(20) NOT NULL AUTO_INCREMENT,
  `nama` varchar(30) NOT NULL,
  `kondisi` text NOT NULL,
  `spesifikasi` text NOT NULL,
  `keterangan` text NOT NULL,
  `jumlah` int(20) NOT NULL,
  `id_jenis` int(20) NOT NULL,
  `tanggal_register` date NOT NULL,
  `id_ruang` int(20) NOT NULL,
  `kode_inventaris` varchar(30) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  `sumber` text NOT NULL,
  PRIMARY KEY (`id_inventaris`),
  KEY `id_jenis` (`id_jenis`),
  KEY `id_ruang` (`id_ruang`),
  KEY `id_petugas` (`id_petugas`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data untuk tabel `inventaris`
--

INSERT INTO `inventaris` (`id_inventaris`, `nama`, `kondisi`, `spesifikasi`, `keterangan`, `jumlah`, `id_jenis`, `tanggal_register`, `id_ruang`, `kode_inventaris`, `id_petugas`, `sumber`) VALUES
(22, 'meja', 'baru', 'ribet', 'adaa', 45, 117, '2019-02-27', 95, 'B0002', 7, 'kepo'),
(23, 'kursi', 'baru', 'baik', 'ada', 20, 117, '2019-02-28', 95, 'B0003', 8, 'suplier'),
(24, 'Laptop', 'baru', 'kepo', 'ada', 25, 116, '2019-03-01', 95, 'B0004', 6, 'suplier'),
(25, 'Papan Tulis', 'baru', 'kepo', 'ada', 20, 117, '2019-03-01', 95, 'B0005', 8, 'suplier'),
(26, 'Alat pemadam', 'baru', 'lama', 'ada', 6, 117, '2019-03-06', 95, 'B0006', 6, 'pembeli');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis`
--

CREATE TABLE IF NOT EXISTS `jenis` (
  `id_jenis` int(20) NOT NULL AUTO_INCREMENT,
  `nama_jenis` varchar(30) NOT NULL,
  `kode_jenis` varchar(20) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=118 ;

--
-- Dumping data untuk tabel `jenis`
--

INSERT INTO `jenis` (`id_jenis`, `nama_jenis`, `kode_jenis`, `keterangan`) VALUES
(116, 'Elektronik', 'A0001', 'good job'),
(117, 'Non Elektronik', 'A0002', 'baik');

-- --------------------------------------------------------

--
-- Struktur dari tabel `level`
--

CREATE TABLE IF NOT EXISTS `level` (
  `id_level` int(50) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(60) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `level`
--

INSERT INTO `level` (`id_level`, `nama_level`) VALUES
(1, 'peminjaman'),
(2, 'admin'),
(3, 'operator');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pegawai`
--

CREATE TABLE IF NOT EXISTS `pegawai` (
  `id_pegawai` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pegawai` varchar(60) NOT NULL,
  `nip` int(45) NOT NULL,
  `alamat` text NOT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data untuk tabel `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `nama_pegawai`, `nip`, `alamat`) VALUES
(6, 'saya', 88, 'laladon'),
(7, 'kamu', 9876, 'laladon');

-- --------------------------------------------------------

--
-- Stand-in structure for view `peminjam`
--
CREATE TABLE IF NOT EXISTS `peminjam` (
`id_inventaris` int(20)
,`id_peminjaman` int(50)
,`nama_pegawai` varchar(60)
,`nama` varchar(30)
,`nama_ruang` varchar(50)
,`jumlah` varchar(30)
,`status_peminjaman` varchar(50)
,`tanggal_pinjam` date
,`tanggal_kembali` date
);
-- --------------------------------------------------------

--
-- Struktur dari tabel `peminjaman`
--

CREATE TABLE IF NOT EXISTS `peminjaman` (
  `id_peminjaman` int(50) NOT NULL AUTO_INCREMENT,
  `tanggal_pinjam` date NOT NULL,
  `tanggal_kembali` date NOT NULL,
  `status_peminjaman` varchar(50) NOT NULL,
  `id_pegawai` int(50) NOT NULL,
  PRIMARY KEY (`id_peminjaman`),
  KEY `id_pegawai` (`id_pegawai`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=47 ;

--
-- Dumping data untuk tabel `peminjaman`
--

INSERT INTO `peminjaman` (`id_peminjaman`, `tanggal_pinjam`, `tanggal_kembali`, `status_peminjaman`, `id_pegawai`) VALUES
(46, '2019-04-06', '0000-00-00', 'pinjam', 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `petugas`
--

CREATE TABLE IF NOT EXISTS `petugas` (
  `id_petugas` int(50) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama_petugas` varchar(50) NOT NULL,
  `id_level` int(50) NOT NULL,
  `baned` enum('N','Y') NOT NULL,
  `logintime` int(10) DEFAULT NULL,
  PRIMARY KEY (`id_petugas`),
  KEY `id_level` (`id_level`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data untuk tabel `petugas`
--

INSERT INTO `petugas` (`id_petugas`, `username`, `email`, `password`, `nama_petugas`, `id_level`, `baned`, `logintime`) VALUES
(6, 'operator', 'puputtthandayani@gmail.com', 'operator', 'opera', 1, 'N', 0),
(7, 'admin', 'hpuput44@gmail.com', 'admin', 'admin', 2, 'N', 0),
(8, 'peminjam', 'nadyaanovaa6@gmail.com', 'peminjaman', 'pinjam', 3, 'N', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ruang`
--

CREATE TABLE IF NOT EXISTS `ruang` (
  `id_ruang` int(50) NOT NULL AUTO_INCREMENT,
  `nama_ruang` varchar(50) NOT NULL,
  `kode_ruang` varchar(50) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id_ruang`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=98 ;

--
-- Dumping data untuk tabel `ruang`
--

INSERT INTO `ruang` (`id_ruang`, `nama_ruang`, `kode_ruang`, `keterangan`) VALUES
(95, 'lab 1', 'C0001', 'bagus'),
(96, 'lab 2', 'C0002', 'baik'),
(97, 'lab 3', 'C0003', 'good job');

-- --------------------------------------------------------

--
-- Struktur untuk view `peminjam`
--
DROP TABLE IF EXISTS `peminjam`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `peminjam` AS select `c`.`id_inventaris` AS `id_inventaris`,`d`.`id_peminjaman` AS `id_peminjaman`,`a`.`nama_pegawai` AS `nama_pegawai`,`c`.`nama` AS `nama`,`e`.`nama_ruang` AS `nama_ruang`,`b`.`jumlah_pinjam` AS `jumlah`,`d`.`status_peminjaman` AS `status_peminjaman`,`d`.`tanggal_pinjam` AS `tanggal_pinjam`,`d`.`tanggal_kembali` AS `tanggal_kembali` from ((((`pegawai` `a` join `detail_pinjam` `b`) join `inventaris` `c`) join `peminjaman` `d`) join `ruang` `e`) where ((`e`.`id_ruang` = `c`.`id_ruang`) and (`a`.`id_pegawai` = `d`.`id_pegawai`) and (`b`.`id_inventaris` = `c`.`id_inventaris`) and (`b`.`id_peminjaman` = `d`.`id_peminjaman`)) order by `d`.`id_peminjaman` desc;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `inventaris`
--
ALTER TABLE `inventaris`
  ADD CONSTRAINT `inventaris_ibfk_1` FOREIGN KEY (`id_jenis`) REFERENCES `jenis` (`id_jenis`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `inventaris_ibfk_2` FOREIGN KEY (`id_ruang`) REFERENCES `ruang` (`id_ruang`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `inventaris_ibfk_3` FOREIGN KEY (`id_petugas`) REFERENCES `petugas` (`id_petugas`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `petugas`
--
ALTER TABLE `petugas`
  ADD CONSTRAINT `petugas_ibfk_1` FOREIGN KEY (`id_level`) REFERENCES `level` (`id_level`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
