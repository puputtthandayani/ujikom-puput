<?php
include 'koneksi.php';
require('pdf/fpdf.php');

$pdf = new FPDF("L","cm","A4");
$tanggal_awal = $_POST['tgl_a'];
$tanggal_akhir = $_POST['tgl_b'];
$pdf->SetMargins(2,1,1);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',11);
$pdf->SetX(4);            
$pdf->MultiCell(19.5,0.5,'INVENTARIS SMK',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'UJIKOM 2019',0,'L');    
$pdf->SetFont('Arial','B',10);
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'SMKN 1 CIOMAS',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'Jalan Raya Laladon No. 2 RT 04/06 Desa Laladon Kec. Ciomas Kab. Bogor',0,'L');
$pdf->Line(1,3.1,28.5,3.1);
$pdf->SetLineWidth(0.1);      
$pdf->Line(1,3.2,28.5,3.2);   
$pdf->SetLineWidth(0);
$pdf->ln(1);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(25.5,0.7,"Laporan Peminjaman ".$tanggal_awal. " sampai ".$tanggal_akhir,0,10,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(5,0.7,"Di cetak pada : ".date("D-d/m/Y"),0,0,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(1, 0.8, 'NO', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Nama Barang', 1, 0, 'C');
$pdf->Cell(2, 0.8, 'Jml Pinjam', 1, 0, 'C');
$pdf->Cell(2.5, 0.8, 'Tgl Pinjam', 1, 0, 'C');
$pdf->Cell(2, 0.8, 'Tgl Kembali', 1, 0, 'C');
$pdf->Cell(2.5, 0.8, 'Status Pinjam', 1, 0, 'C');
$pdf->Cell(2.5, 0.8, 'Peminjam', 1, 1, 'C');
$pdf->SetFont('Arial','',10);
$no=1;

$data = mysqli_query($koneksi,"SELECT * from peminjam  order by id_peminjaman desc");

while($lihat=mysqli_fetch_array($data)){
	$pdf->Cell(1, 0.8, $no , 1, 0, 'C');
	$pdf->Cell(3, 0.8, $lihat['nama'],1, 0, 'C');
	$pdf->Cell(2, 0.8, $lihat['jumlah'], 1, 0,'C');
	$pdf->Cell(2.5, 0.8, $lihat['tanggal_pinjam'],1, 0, 'C');
	$pdf->Cell(2, 0.8, $lihat['tanggal_kembali'], 1, 0,'C');
	$pdf->Cell(2.5, 0.8, $lihat['status_peminjaman'],1, 0, 'C');
	$pdf->Cell(2.5, 0.8, $lihat['nama_pegawai'], 1, 1,'C');



	$no++;
}

$pdf->Output("cetak_barang.pdf","I");

?>

